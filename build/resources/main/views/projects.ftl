<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Projects and Branches </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <script src="../../app.js" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>



<h1 class="bg-primary text-white" style="text-align: center; padding-bottom: 5%;">Total amount of Projects: ${projectAndBranches?size}</h1>

<div id="emailDiv" class="container" style="padding: 5%">
    Emails: >${userMails}
</div>
<div class="container">

    <div  class="custom-control custom-checkbox" id="myCheckbox" >
        <input type="checkbox" class="custom-control-input" id="customCheck1" checked>
        <label class="custom-control-label" for="customCheck1">Show archived Projects</label>
    </div>
    <div  class="custom-control custom-checkbox" id="myCheckbox2">
        <input type="checkbox" class="custom-control-input" id="customCheck2" checked>
        <label class="custom-control-label" for="customCheck2">Show non archived Projects</label>
    </div>
    <div  class="custom-control custom-checkbox" id="myCheckbox3">
        <input type="checkbox" class="custom-control-input" id="customCheck3">
        <label class="custom-control-label" for="customCheck3">Darkmode</label>
    </div>
    <div  class="custom-control custom-checkbox" id="myCheckbox4">
        <input type="checkbox" class="custom-control-input" id="customCheck4">
        <label class="custom-control-label" for="customCheck4">Show Emails</label>
    </div>
</div>
<br>

<div style="margin-bottom: 5em; margin-top: 5em" >

</div>


<div style="display: grid; grid-template-columns: auto auto auto auto; padding: 10px;">

    <#list projectAndBranches as project>
        <div style="padding: 20px;">


            <button name="${project.archived?string}Status" style="width: 100%;" class="btn btn-primary" type="button" data-toggle="collapse" data-target="#${project.name}-target" aria-expanded="false" >
                <#if project.storageSize gt 400>
                    <img src="../../red.png" alt="" style="width: 2em; height: 2em;">
                <#elseif project.storageSize gt 200>
                    <img src="../../yellow.png" alt="" style="width: 2em; height: 2em;">
                </#if>
                Project: ${project.name}
                <#if project.branches?size gt 20>
                    <span class="badge badge-danger">Branches: ${project.branches?size}</span>
                <#elseif project.branches?size gt 10>
                    <span class="badge badge-warning">Branches: ${project.branches?size}</span>
                <#else>
                    <span class="badge badge-success">Branches: ${project.branches?size}</span>
                </#if>
                <#if project.archived = true>
                    <img src="../../archived.png" alt="" style="width: 2em; height: 2em;">
                </#if>


            </button>

            <div class="collapse" id="${project.name}-target" style="color: black">
                <div class="card card-body">

                    <#if project.archived = true>
                        <b>Archived: true</b>
                    <#elseif project.archived= false>
                        <b>Archived: false</b>
                    </#if>
                    <b>URL: <a href="${project.url}">  ${project.url} </a></b>
                    <b>ID: ${project.id}</b>
                    <b>Last Activity: ${project.lastActivityAt}</b>
                    <b>Storage Size: ${project.storageSize} MB</b>
                    <#list project.branches as branch>
                        ${branch.name}
                        <br>

                    </#list>
                </div>
            </div>
        </div>
    </#list>

</div>

</body>
</html>

