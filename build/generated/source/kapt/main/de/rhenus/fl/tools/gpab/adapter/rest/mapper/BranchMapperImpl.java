package de.rhenus.fl.tools.gpab.adapter.rest.mapper;

import javax.annotation.Generated;
import javax.inject.Named;
import javax.inject.Singleton;
import org.gitlab4j.api.models.Branch;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-04-12T13:12:14+0200",
    comments = "version: 1.4.0.Final, compiler: javac, environment: Java 1.8.0_202 (AdoptOpenJdk)"
)
@Singleton
@Named
public class BranchMapperImpl implements BranchMapper {

    @Override
    public de.rhenus.fl.tools.gpab.application.model.Branch mapBranch(Branch getBranches) {
        if ( getBranches == null ) {
            return null;
        }

        de.rhenus.fl.tools.gpab.application.model.Branch branch = new de.rhenus.fl.tools.gpab.application.model.Branch();

        branch.setName( getBranches.getName() );

        return branch;
    }
}
