package de.rhenus.fl.tools.gpab.adapter.rest.mapper;

import java.text.SimpleDateFormat;
import javax.annotation.Generated;
import javax.inject.Named;
import javax.inject.Singleton;
import org.gitlab4j.api.models.Project;
import org.gitlab4j.api.models.ProjectStatistics;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2021-04-12T13:12:14+0200",
    comments = "version: 1.4.0.Final, compiler: javac, environment: Java 1.8.0_202 (AdoptOpenJdk)"
)
@Singleton
@Named
public class ProjectMapperImpl implements ProjectMapper {

    @Override
    public de.rhenus.fl.tools.gpab.application.model.Project mapProject(Project getProject) {
        if ( getProject == null ) {
            return null;
        }

        de.rhenus.fl.tools.gpab.application.model.Project project = new de.rhenus.fl.tools.gpab.application.model.Project();

        project.setName( getProject.getName() );
        if ( getProject.getId() != null ) {
            project.setId( String.valueOf( getProject.getId() ) );
        }
        project.setUrl( getProject.getWebUrl() );
        if ( getProject.getLastActivityAt() != null ) {
            project.setLastActivityAt( new SimpleDateFormat().format( getProject.getLastActivityAt() ) );
        }
        project.setStorageSize( getProjectStatisticsStorageSize( getProject ) );
        project.setArchived( getProject.getArchived() );

        return project;
    }

    private Long getProjectStatisticsStorageSize(Project project) {
        if ( project == null ) {
            return null;
        }
        ProjectStatistics statistics = project.getStatistics();
        if ( statistics == null ) {
            return null;
        }
        long storageSize = statistics.getStorageSize();
        return storageSize;
    }
}
