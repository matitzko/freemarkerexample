package de.rhenus.fl.tools.gpab.port.in;

import de.rhenus.fl.tools.gpab.application.model.Branch;
import de.rhenus.fl.tools.gpab.application.model.Project;
import de.rhenus.fl.tools.gpab.application.model.User;
import org.gitlab4j.api.GitLabApiException;

import java.util.HashMap;
import java.util.List;

public interface BranchInformationPort {

    List<Branch> getBranches(int id) throws GitLabApiException;
    List<Project> getProjectBranches();
    Long getStorageSize(int id);
    List<User> userList();

}

