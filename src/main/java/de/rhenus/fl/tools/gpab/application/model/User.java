package de.rhenus.fl.tools.gpab.application.model;

import io.micronaut.core.annotation.Introspected;

@Introspected
public class User {
    String userName;
    String email;
    String publicEmail;

    public User(String userName, String email, String publicEmail) {
        this.userName = userName;
        this.email = email;
        this.publicEmail = publicEmail;
    }

    public String getPublicEmail() {
        return publicEmail;
    }

    public void setPublicEmail(String publicEmail) {
        this.publicEmail = publicEmail;
    }



    public User() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
