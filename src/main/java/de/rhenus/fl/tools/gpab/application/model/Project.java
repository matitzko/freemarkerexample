package de.rhenus.fl.tools.gpab.application.model;

import io.micronaut.core.annotation.Introspected;

import java.util.Date;
import java.util.List;

@Introspected
public class Project {

    String name;
    String id;
    List<Branch> branches;
    String url;
    String lastActivityAt;
    Long storageSize;
    Boolean archived;

    public Boolean getArchived() {
        return archived;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public Long getStorageSize() {
        return storageSize;
    }

    public void setStorageSize(Long storageSize) {
        this.storageSize = storageSize;
    }

    public String getLastActivityAt() {
        return lastActivityAt;
    }

    public void setLastActivityAt(String lastActivityAt) {
        this.lastActivityAt = lastActivityAt;
    }





    public List<Branch> getBranches() { return branches;}

    public void setBranches(List<Branch> branches) { this.branches = branches;}

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
