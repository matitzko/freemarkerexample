package de.rhenus.fl.tools.gpab.adapter.rest.mapper;

import de.rhenus.fl.tools.gpab.application.model.Project;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;


@Mapper
public interface ProjectMapper {

    @Mapping(source="name", target = "name")
    @Mapping(source="id",target = "id")
    @Mapping(source="webUrl",target = "url")
    @Mapping(target = "branches", ignore = true)
    @Mapping(source = "lastActivityAt", target="lastActivityAt")
    @Mapping(source="statistics.storageSize", target = "storageSize")
    @Mapping(source="archived", target="archived")
//    @Mapping(source="repositoryStorage",target="repositoryStorage")
//    @Mapping(source="avatarUrl", target="avatarUrl")
    Project mapProject(org.gitlab4j.api.models.Project getProject);








}
