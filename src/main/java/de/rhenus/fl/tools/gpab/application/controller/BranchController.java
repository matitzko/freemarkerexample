package de.rhenus.fl.tools.gpab.application.controller;

import de.rhenus.fl.tools.gpab.application.model.Branch;
import de.rhenus.fl.tools.gpab.application.model.Project;
import de.rhenus.fl.tools.gpab.application.model.User;
import de.rhenus.fl.tools.gpab.port.in.BranchInformationPort;
import de.rhenus.fl.tools.gpab.port.out.OutportGitlab;
import org.gitlab4j.api.GitLabApiException;

import javax.inject.Inject;
import java.util.List;

public class BranchController implements BranchInformationPort {

    private OutportGitlab outportGitlab;

    @Inject
    public BranchController(OutportGitlab outportGitlab) {
        this.outportGitlab = outportGitlab;
    }


    @Override
    public List<Branch> getBranches(int id) throws GitLabApiException {
        return outportGitlab.getBranches(id);
    }

    @Override
    public List<Project> getProjectBranches() {
        return outportGitlab.getProjectBranches();
    }

    @Override
    public Long getStorageSize(int id) {
        return outportGitlab.getStorageSize(id);
    }

    @Override
    public List<User> userList() {
        return outportGitlab.getUserList();
    }
}
