package de.rhenus.fl.tools.gpab.adapter.rest.mapper;

import de.rhenus.fl.tools.gpab.application.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface UserMapper {

    @Mapping(source="name", target="userName")
    @Mapping(source="email", target="email")
    @Mapping(source= "publicEmail", target="publicEmail")
    User mapUser(org.gitlab4j.api.models.User getUser);

}
