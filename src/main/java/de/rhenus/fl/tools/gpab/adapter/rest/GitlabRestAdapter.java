package de.rhenus.fl.tools.gpab.adapter.rest;

import de.rhenus.fl.tools.gpab.adapter.rest.mapper.BranchMapper;
import de.rhenus.fl.tools.gpab.adapter.rest.mapper.ProjectMapper;
import de.rhenus.fl.tools.gpab.adapter.rest.mapper.UserMapper;
import de.rhenus.fl.tools.gpab.application.model.Branch;
import de.rhenus.fl.tools.gpab.application.model.Project;
import de.rhenus.fl.tools.gpab.port.out.OutportGitlab;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.GitLabApiException;
import org.gitlab4j.api.Pager;
import org.gitlab4j.api.models.Contributor;
import org.gitlab4j.api.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.util.*;
import java.util.stream.Collectors;

public class GitlabRestAdapter implements OutportGitlab {


//
    private GitLabApi gitLabApi;
    private ProjectMapper projectMapper;
    private BranchMapper branchMapper;
    private UserMapper userMapper;
    Logger logger = LoggerFactory.getLogger(GitlabRestAdapter.class);
    private RxHttpClient rxHttpClient;

    @Inject
    public GitlabRestAdapter(@Client("${gitlab}") RxHttpClient rxHttpClient, @Value("${gitlab}") String url,@Value("${client.token}") String token, ProjectMapper projectMapper, BranchMapper branchMapper, UserMapper userMapper) throws GitLabApiException {
        this.rxHttpClient=rxHttpClient;
        this.projectMapper=projectMapper;
        this.branchMapper = branchMapper;
        this.userMapper =  userMapper;
        gitLabApi = new GitLabApi(url, token);
        gitLabApi.setIgnoreCertificateErrors(true);



    }

    public List<Branch> getBranches(int id){
        try {
            List<org.gitlab4j.api.models.Branch> branches = gitLabApi.getRepositoryApi().getBranches(id);
            return branches.stream().map(branch -> branchMapper.mapBranch(branch)).collect(Collectors.toList());
        }catch(GitLabApiException e){
            logger.error("Getting the Branches failed: " + e.getMessage());
        }
        return Collections.emptyList();
    }



    @Override
    public List<Project> getProjectBranches() {
        try {

            Pager<org.gitlab4j.api.models.Project> pager = gitLabApi.getProjectApi().getProjects(20);



            return pager.all().
                    stream().map(project -> {
                Project mappedProject= projectMapper.mapProject(project);
                mappedProject.setBranches(getBranches(Integer.parseInt(mappedProject.getId())));
                mappedProject.setStorageSize(getStorageSize(Integer.parseInt(mappedProject.getId()))/1024/1024);
                return mappedProject;
            }).sorted((p1,p2)->(p1.getBranches().size()-p2.getBranches().size())*-1).collect(Collectors.toList());
        }catch (GitLabApiException e){
            logger.error("getting the Projects and Branches failed: " + e.getMessage());
        }
        return Collections.emptyList();
    }

    @Override
    public Long getStorageSize(int id) {
        try {
            return gitLabApi.getProjectApi().getProject(id,true).getStatistics().getStorageSize();
        } catch (GitLabApiException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<de.rhenus.fl.tools.gpab.application.model.User> getUserList() {
        try {
            List<User> activeUsers = gitLabApi.getUserApi().getActiveUsers();
            return activeUsers.stream().map(user -> userMapper.mapUser(user)).collect(Collectors.toList());
        }catch (GitLabApiException e){
            e.printStackTrace();
        }
        return null;
    }


}
