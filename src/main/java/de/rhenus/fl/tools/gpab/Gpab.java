package de.rhenus.fl.tools.gpab;

import io.micronaut.runtime.Micronaut;

public class Gpab {

    public static void main(String[] args){

        Micronaut.run(Gpab.class);
    }
}
