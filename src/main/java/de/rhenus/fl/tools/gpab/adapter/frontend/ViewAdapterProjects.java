package de.rhenus.fl.tools.gpab.adapter.frontend;

import de.rhenus.fl.tools.gpab.application.controller.BranchController;
import de.rhenus.fl.tools.gpab.application.model.Project;
import de.rhenus.fl.tools.gpab.application.model.User;
import io.micronaut.core.util.CollectionUtils;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.views.View;

import javax.inject.Inject;
import java.util.List;


@Controller("/view")
public class ViewAdapterProjects {

    private BranchController branchController;

    @Inject
    public ViewAdapterProjects(BranchController branchController) {
        this.branchController = branchController;
    }



    @View("projects")
    @Get(value="/projects")
    public HttpResponse projects(){
        List<Project> projectAndBranches = branchController.getProjectBranches();
        String userMails ="";
        List<User> userList = branchController.userList();
        for(User user: userList){
            userMails=userMails+ user.getEmail() + "; ";
        }
        return HttpResponse.ok(CollectionUtils.mapOf("projectAndBranches",projectAndBranches, "userMails", userMails));
    }





}
