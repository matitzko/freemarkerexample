package de.rhenus.fl.tools.gpab.adapter.rest.mapper;

import de.rhenus.fl.tools.gpab.application.model.Project;
import org.gitlab4j.api.models.Branch;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface BranchMapper {

    @Mapping(source="name", target = "name")
    de.rhenus.fl.tools.gpab.application.model.Branch mapBranch(org.gitlab4j.api.models.Branch getBranches);



}
