docker pull fl-swarm.rhenus.de:4444/gitlabprojectsandbranches   -> Pulls the image

docker run -d -p 8080:8181 fl-swarm.rhenus.de:4444/gitlabprojectsandbranches  -> Starts the container

Access the application: https://localhost:8080/view/projects     -> be patient, it might take 2-3 minutes until it is loaded
